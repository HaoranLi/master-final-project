#!/usr/bin/python
import os

import rospy
import actionlib

from lmtest import LMTest

from threading import Thread
from Queue import Queue
from play_wav import play_wav
from lasr_speech.msg import informationResult, informationAction
from std_msgs.msg import String



SOUNDS_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/sounds/'
GRAMMAR_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/dict/'
ACOUSTIC_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/models/'


class SpeechServer(object):
    def __init__(self):
        self.flag_queue = Queue()
        self.lm_test = None
        self.grammar = None
        self.speaker = None
        self.food = ''
        self.drink = ''
        self.order = ''
        self._result = informationResult()
        self.server = actionlib.SimpleActionServer('serve_food', informationAction, execute_cb=self.execute_cb, auto_start=False)
        self.server.start()

    def handle_serve_food(self):
        #get the name
        rospy.loginfo("Waiting for command")
        detected_info1 = rospy.wait_for_message("lm_data", String).data
        rospy.loginfo(detected_info1)
        self._result.data = detected_info1

    def handle_choose_action(self):
        detected_info1 = rospy.wait_for_message("lm_data", String).data
        rospy.loginfo(detected_info1)
        if detected_info1.find("EAT") > -1:
            self._result.data = "Order_food"
        elif detected_info1.find("DRINK") > -1:
            self._result.data = "Order_drink"
        elif detected_info1.find("NO,THANKS") > -1:
            self.order = self.food + " and " + self.drink
            self._result.data = self.order

    def handle_order_food(self):
        detected_info1 = rospy.wait_for_message("lm_data", String).data
        rospy.loginfo(detected_info1)
        if detected_info1.find("SANDWICH") > -1:
            self.food = "sandwich"
            self._result.data = "Ask_for_action"
        elif detected_info1.find("BERRY SMOOTHIE") > -1:
            self.food = "berry smoothie"
            self._result.data = "Ask_for_action"
        elif detected_info1.find("MANGO SMOOTHIE") > -1:
            self.food = "mango smoothie"
            self._result.data = "Ask_for_action"

    def handle_order_drink(self):
        detected_info1 = rospy.wait_for_message("lm_data", String).data
        rospy.loginfo(detected_info1)
        if detected_info1.find("COFFEE") > -1:
            self.drink = "coffee"
            self._result.data = "Ask_for_action"
        elif detected_info1.find("WATER") > -1:
            self.drink = "water"
            self._result.data = "Ask_for_action"

    def handle_ask_location(self):
        if self.speaker == 'Guest':
            self._result.data = "Guest_place"
        else:
            detected_info1 = rospy.wait_for_message("lm_data", String).data
            rospy.loginfo(detected_info1)
            if detected_info1.find("MY OFFICE") > -1:
                self._result.data = "my_office"
            elif detected_info1.find("MATTEO'S OFFICE") > -1:
                self._result.data = "Matteo"


    def load_grammar(self, grammar):
        # kill the previous lm_test
        if self.lm_test is not None:
            self.flag_queue.put(1)
            while not self.flag_queue.empty():
                rospy.sleep(1)
                print 'waiting to kill lm_test...'
            self.lm_test = None

        # load the new grammar and run the lm_test
        self.grammar = grammar
        lm_path = GRAMMAR_PATH + grammar + '.lm'
        dict_path = GRAMMAR_PATH + grammar + '.dic'
        hmm_path = ACOUSTIC_PATH + self.speaker
        self.lm_test = Thread(target=LMTest, kwargs={'flag_queue':self.flag_queue, '_hmm_param':hmm_path, '_lm_param':lm_path, '_dict_param':dict_path})
        self.lm_test.start()

    def execute_cb(self, goal):
        # load the dict specified by the goal
        if not goal.action == 'New_speaker':
            self.load_grammar(goal.grammar)
        play_wav(SOUNDS_PATH + 'bell.wav')
        # use the goal to choose how to handle this one
        if goal.action == 'New_speaker':
            self.speaker = goal.grammar
            self._result.data = "Ask_for_action"
        elif goal.action == 'Choose_action':
            self.handle_choose_action()
        elif goal.action == 'Choose_food':
            self.handle_order_food()
        elif goal.action == 'Choose_drink':
            self.handle_order_drink()
        elif goal.action == 'Delivery':
            self.handle_ask_location()

        self.server.set_succeeded(self._result)
        print("Information successfully passed to client!")


if __name__ == '__main__':
    rospy.init_node('serve_food_server')
    SpeechServer()
    rospy.spin()
