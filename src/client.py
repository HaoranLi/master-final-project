#!/usr/bin/env python3
import rospy
import actionlib
from lasr_speech.msg import informationAction, informationGoal
import speaker_recognition
#from sound_play.libsoundplay import SoundClient
from std_msgs.msg import String
import subprocess
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

def talk(text, wait=False):
    print('\033[1;36mTIAGO: ' + text + '\033[0m')
    tts_proc = subprocess.Popen(['echo "{}" | festival --tts'.format(text)], shell=True)
    if wait == True:
        tts_proc.wait()

class FoodServerClient(object):
    def __init__(self):
        self.client = actionlib.SimpleActionClient('serve_food', informationAction)
        rospy.init_node('serve_food_client')
        self.result = ''
        self.target = ''
        self.order = ''
        self.speaker = speaker_recognition.my_record()
        print(self.speaker)
        while self.target == '':
            rospy.sleep(1)
            self.SendGoal()
        if self.target == 'Haoran':
            move_client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
            move_client.wait_for_server()
            mc_goal = MoveBaseGoal()
            mc_goal.target_pose.header.frame_id = "map"
            mc_goal.target_pose.pose.position.x = -5.18
            mc_goal.target_pose.pose.position.y = 3.12
            mc_goal.target_pose.pose.position.z = 0
            mc_goal.target_pose.pose.orientation.w = 0
            mc_goal.target_pose.pose.orientation.x = 0
            mc_goal.target_pose.pose.orientation.y = 0
            mc_goal.target_pose.pose.orientation.z = 1
            move_client.send_goal(mc_goal)
            result = move_client.wait_for_result()
            if result:
                talk("Here is your stuffs", wait=True)
        elif self.target == 'Matteo':
            move_client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
            move_client.wait_for_server()
            mc_goal = MoveBaseGoal()
            mc_goal.target_pose.header.frame_id = "map"
            mc_goal.target_pose.pose.position.x = -1.42
            mc_goal.target_pose.pose.position.y = -2.1
            mc_goal.target_pose.pose.position.z = 0
            mc_goal.target_pose.pose.orientation.w = 0
            mc_goal.target_pose.pose.orientation.x = 0
            mc_goal.target_pose.pose.orientation.y = 0
            mc_goal.target_pose.pose.orientation.z = 1
            move_client.send_goal(mc_goal)
            result = move_client.wait_for_result()
            if result:
                talk("Here is your stuffs", wait=True)
        elif self.target == 'Cecilia':
            move_client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
            move_client.wait_for_server()
            mc_goal = MoveBaseGoal()
            mc_goal.target_pose.header.frame_id = "map"
            mc_goal.target_pose.pose.position.x = 2.62
            mc_goal.target_pose.pose.position.y = 6.47
            mc_goal.target_pose.pose.position.z = 0
            mc_goal.target_pose.pose.orientation.w = 0
            mc_goal.target_pose.pose.orientation.x = 0
            mc_goal.target_pose.pose.orientation.y = 0
            mc_goal.target_pose.pose.orientation.z = 1
            move_client.send_goal(mc_goal)
            result = move_client.wait_for_result()
            if result:
                talk("Here is your stuffs", wait=True)
        elif self.target == 'Guest':
            talk("Sorry, you can only eat in the lobby", wait=True)

    def SendGoal(self):
        name = self.speaker
        if self.result == '':
            if self.speaker == 'Guest':
                talk("Hello, I haven't meet you before", wait=True)
            else:
                talk("Hello, " + name, wait=True)
                talk("It's nice to see you again!", wait=True)
            self.goal = informationGoal(name, 'New_speaker')
            self.client.send_goal(self.goal)
            self.client.wait_for_result()
            self.result = self.client.get_result().data
            print(self.result)
        elif self.result == 'Ask_for_action':
            if self.order == '':
                talk("What can I do for you ?", wait=True)
            else:
                talk("Anything else ?", wait=True)
            self.goal = informationGoal('actions', 'Choose_action')
            self.client.send_goal(self.goal)
            self.client.wait_for_result()
            self.result = self.client.get_result().data
            print(self.result)
        elif self.result == 'Order_food':
            talk("I have sandwich, berry smoothie and mango smoothie", wait=True)
            talk("What do you want?", wait=True)
            self.goal = informationGoal('food', 'Choose_food')
            self.client.send_goal(self.goal)
            self.client.wait_for_result()
            self.result = self.client.get_result().data
            self.order = 'Got'
            print(self.result)
        elif self.result == 'Order_drink':
            talk("I have water and coffee", wait=True)
            talk("What do you want?", wait=True)
            self.goal = informationGoal('drink', 'Choose_drink')
            self.client.send_goal(self.goal)
            self.client.wait_for_result()
            self.result = self.client.get_result().data
            self.order = 'Got'
            print(self.result)
        else:
            if not self.result == 'Ask_delivery':
                talk("Okay, you ordered " + self.result, wait=True)
            talk("Where do you want me to send?", wait=True)
            if self.result == 'Guest_place':
                self.target = 'Guest'
            else:
                self.goal = informationGoal('location', 'Delivery')
                self.client.send_goal(self.goal)
                self.client.wait_for_result()
                self.result = self.client.get_result().data
                print(self.result)
                if self.result == 'my_office':
                    self.target = self.speaker
                elif self.result == 'Guest_place':
                    self.target = 'Guest'
                else:
                    talk("Permission denied! You are not him!", wait=True)
                    self.result = 'Ask_delivery'



if __name__ == '__main__':
    FoodServerClient()
    rospy.spin()
