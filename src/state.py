#!/usr/bin/env python3
import rospy
import smach
import actionlib
from lasr_speech.msg import informationAction, informationGoal
import speaker_recognition
import subprocess
#import smach_ros
def speech_runnable(result_dict):
    self.client = actionlib.SimpleActionClient('serve_food', informationAction)
    rospy.init_node('serve_food_client')
def talk(text, wait=False):
    print('\033[1;36mTIAGO: ' + text + '\033[0m')
    tts_proc = subprocess.Popen(['echo "{}" | festival --tts'.format(text)], shell=True)
    if wait == True:
        tts_proc.wait()


class Initial(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1'])
        self.speaker = ''

    def execute(self, userdata):
        rospy.loginfo('Executing state Initial')
        talk('Hello, what can I do for you', wait=True)
        self.speaker = 'Haoran'
        if not self.speaker == '':
            return 'outcome1'

class Ask_action(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome2', 'outcome3', 'outcome4'])
        self.action = ''

    def execute(self, userdata):
        rospy.loginfo('Executing state Ask_action')
        rospy.sleep(2)
        self.action = 'delivery'
        if self.action == 'food':
            return 'outcome2'
        elif self.action == 'drink':
            return 'outcome3'
        elif self.action == 'delivery':
            return 'outcome4'

class Order_food(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1'])
        self.food = ''
    def execute(self, userdata):
        rospy.loginfo('Executing state Order_food')
        rospy.sleep(2)
        self.food = 'sandwich'
        if not self.food == '':
            return 'outcome1'

class Order_drink(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1'])
        self.drink = ''
    def execute(self, userdata):
        rospy.loginfo('Executing state Order_drink')
        rospy.sleep(2)
        self.drink = 'water'
        if not self.drink == '':
            return 'outcome1'

class Ask_delivery(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome0'])
        self.goal = ''
    def execute(self, userdata):
        rospy.loginfo('Executing state Delivery')
        rospy.sleep(2)
        self.goal = 'Haoran'
        if not self.goal == '':
            return 'outcome0'

def main():
    sm = smach.StateMachine(outcomes=['outcome6'])
    with sm:
        smach.StateMachine.add('Initial', Initial(), transitions={'outcome1':'Ask_action'})
        smach.StateMachine.add('Ask_action', Ask_action(), transitions={'outcome2':'Order_food', 'outcome3':'Order_drink', 'outcome4':'Ask_delivery'})
        smach.StateMachine.add('Order_food', Order_food(), transitions={'outcome1':'Ask_action'})
        smach.StateMachine.add('Order_drink', Order_drink(), transitions={'outcome1':'Ask_action'})
        smach.StateMachine.add('Ask_delivery', Ask_delivery(), transitions={'outcome0':'Initial'})

    #sis = smach_ros.IntrospectionServer('my_smach_introspection_server', sm, '/SM_ROOT')
    #sis.start()
    outcome = sm.execute()
    rospy.spin()
    #sis.stop()
if __name__ == '__main__':
    main()
