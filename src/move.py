#!/usr/bin/python
import rospy
import actionlib

# import these to create move base goals
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

def movebase_client():
    rospy.init_node('move_client')
    move_client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    move_client.wait_for_server()
    mc_goal = MoveBaseGoal()
    mc_goal.target_pose.header.frame_id = "map"
    mc_goal.target_pose.pose.position.x = -5.18
    mc_goal.target_pose.pose.position.y = 3.12
    mc_goal.target_pose.pose.position.z = 0
    mc_goal.target_pose.pose.orientation.w = 0
    mc_goal.target_pose.pose.orientation.x = 0
    mc_goal.target_pose.pose.orientation.y = 0
    mc_goal.target_pose.pose.orientation.z = 1
    move_client.send_goal(mc_goal)
    wait = move_client.wait_for_result()
    if not wait:
        rospy.logerr("Action server not available!")
        rospy.signal_shutdown("Action server not available!")
    else:
        return client.get_result()

if __name__ == '__main__':
    try:
        result = movebase_client()
        if result:
            rospy.loginfo("Goal execution done!")
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.")
