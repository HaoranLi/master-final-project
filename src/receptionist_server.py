#!/usr/bin/python
import os

import rospy
import actionlib

from lmtest import LMTest

from threading import Thread
from Queue import Queue

from lasr_speech.msg import informationResult, informationAction
from std_msgs.msg import String
#from sound_play.libsoundplay import SoundClient
#soundhandle = SoundClient()

GRAMMAR_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/dict/'


class SpeechServer(object):
    def __init__(self):
        self.flag_queue = Queue()
        self.lm_test = None
        self.grammar = None
        self._result = informationResult()
        self.server = actionlib.SimpleActionServer('serve_food', informationAction, execute_cb=self.execute_cb, auto_start=False)
        self.server.start()

    def handle_serve_food(self):
        #get the name
        detected_info1 = rospy.wait_for_message("lm_data", String).data
        if detected_info1.data.find("CHECK"):
            rospy.loginfo("Checking the items")
            rospy.sleep(2)
            rospy.loginfo("Items checked")
            self._result.data ='Item_checked'
        elif detected_info1.data.find("All SET"):
            rospy.loginfo("Serving the food")
            self.load_grammar(goal.grammar)
            detected_info1 = rospy.wait_for_message("lm_data", String).data
            self.loginfo("Waiting for customer to check his food")
            if detected_info1.data.find("THANK YOU"):
                self._result.data = 'Food_delivered'

    def handle_get_order(self):
        # voice cmd starter
        rospy.loginfo("The customer wants coffee")
        self._result.data = 'Order'

    def load_grammar(self, grammar):
        # kill the previous lm_test
        if self.lm_test is not None:
            self.flag_queue.put(1)
            while not self.flag_queue.empty():
                rospy.sleep(1)
                print 'waiting to kill lm_test...'
            self.lm_test = None

        # load the new grammar and run the lm_test
        self.grammar = grammar
        lm_path = GRAMMAR_PATH + grammar + '.lm'
        dict_path = GRAMMAR_PATH + grammar + '.dict'
        self.lm_test = Thread(target=LMTest, kwargs={'flag_queue':self.flag_queue, '_lm_param':lm_path, '_dict_param':dict_path})
        self.lm_test.start()


    def execute_cb(self, goal):
        # load the dict specified by the goal
        #if not goal.grammar == self.grammar:
        self.load_grammar(goal.grammar)

        # use the goal to choose how to handle this one
        if goal.action == 'Ask_Order':
            self.handle_get_order()
        elif goal.action == 'Check_items':
            self.handle_serve_food()
        elif goal.action == 'All_set':
            self.handle_serve_food()

        self.server.set_succeeded(self._result)
        print("Information successfully passed to client!")


if __name__ == '__main__':
    rospy.init_node('serve_food_server')
    SpeechServer()
    rospy.spin()
