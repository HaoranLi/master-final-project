#!/usr/bin/python

import os
import rospy

from Queue import Queue

from std_msgs.msg import String
from pocketsphinx.pocketsphinx import *
from sphinxbase.sphinxbase import *

LASR_SPEECH_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class LMTest(object):
    """Class to add lm functionality."""

    def __init__(self, flag_queue = Queue(), \
                       _lm_param= LASR_SPEECH_PATH + '/dict/receptionist.lm', \
                       _dict_param= LASR_SPEECH_PATH + '/dict/receptionist.dic', \
                       _hmm_param=':default', \
                       _gram=':default', \
                       _rule=':default', \
                       _grammar=':default'):

        self.flag_queue = flag_queue

        # Initializing publisher with buffer size of 10 messages
        self.pub_ = rospy.Publisher("lm_data", String, queue_size=10)
        # initialize node
        # rospy.init_node("lm_control")
        # Call custom function on node shutdown
        rospy.on_shutdown(self.shutdown)

        # check if lm or grammar mode. Default = grammar
        self._use_lm = 1

        self.in_speech_bf = False

        # Setting param values
        self.hmm = _hmm_param
        if _hmm_param == ":default":
            if os.path.isdir(LASR_SPEECH_PATH + "/models"):
                rospy.loginfo("Loading the default acoustic model")
                self.hmm = LASR_SPEECH_PATH + "/models/haoran"
                rospy.loginfo("Done loading the default acoustic model")
            else:
                rospy.logerr(
                    "No language model specified. Couldn't find default model.")
                return

        if _dict_param != ":default":
            self.dict = _dict_param
        else:
            rospy.logerr(
                "No dictionary found. Please add an appropriate dictionary argument.")
            return

        if _lm_param != ':default':
            self._use_lm = 1
            self.class_lm = _lm_param
        else:
            self._use_lm = 0
            self.gram = _gram
            self.rule = _rule

        # All params satisfied. Starting recognizer
        self.start_recognizer()

    def start_recognizer(self):
        """Function to handle lm or grammar processing of audio."""
        config = Decoder.default_config()
        rospy.loginfo("Done initializing pocketsphinx")

        # Setting configuration of decoder using provided params
        config.set_string('-hmm', self.hmm)
        config.set_string('-dict', self.dict)

        # Check if language model to be used or grammar mode
        if self._use_lm:
            rospy.loginfo('Language Model Found.')
            config.set_string('-lm', self.class_lm)
            self.decoder = Decoder(config)
        else:
            rospy.loginfo(
                'language model not found. Using JSGF grammar instead.')
            self.decoder = Decoder(config)

            # Switch to JSGF grammar
            jsgf = Jsgf(self.gram + '.gram')
            rule = jsgf.get_rule(_grammar + '.' + self.rule)
            # Using finite state grammar as mentioned in the rule
            rospy.loginfo(_grammar + '.' + self.rule)
            fsg = jsgf.build_fsg(rule, self.decoder.get_logmath(), 7.5)
            rospy.loginfo("Writing fsg to " +
                          self.gram + '.fsg')
            fsg.writefile(self.gram + '.fsg')

            self.decoder.set_fsg(self.gram, fsg)
            self.decoder.set_search(self.gram)

        # Start processing input audio
        self.decoder.start_utt()
        rospy.loginfo("Decoder started successfully")

        # Subscribe to audio topic
        self.sub = rospy.Subscriber("sphinx_audio", String, self.process_audio)

    def process_audio(self, data):
        # print self.class_lm
        if not self.flag_queue.empty():
            self.flag_queue.get()
            self.sub.unregister()
        """Audio processing based on decoder config."""
        # Check if input audio has ended
        self.decoder.process_raw(data.data, False, False)
        if self.decoder.get_in_speech() != self.in_speech_bf:
            self.in_speech_bf = self.decoder.get_in_speech()
            if not self.in_speech_bf:
                self.decoder.end_utt()
                if self.decoder.hyp() != None:
                    rospy.loginfo('OUTPUT: \"' + self.decoder.hyp().hypstr + '\"')
                    self.pub_.publish(self.decoder.hyp().hypstr)
                self.decoder.start_utt()

    @staticmethod
    def shutdown():
        """This function is executed on node shutdown."""
        # command executed after Ctrl+C is pressed
        rospy.loginfo("Stop LMControl")
        rospy.sleep(1)
