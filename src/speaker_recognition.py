#!/usr/bin/env python3
import os
from recognition import task_predict
import wave
from pyaudio import PyAudio,paInt16
LASR_SPEECH_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

framerate=8000
NUM_SAMPLES=2000
channels=1
sampwidth=2
TIME=2
def save_wave_file(filename,data):
    '''save the date to the wavfile'''
    wf=wave.open(filename,'wb')
    wf.setnchannels(channels)
    wf.setsampwidth(sampwidth)
    wf.setframerate(framerate)
    wf.writeframes(b"".join(data))
    wf.close()
def my_record():
    pa=PyAudio()
    stream=pa.open(format = paInt16,channels=1,
                   rate=framerate,input=True,
                   frames_per_buffer=NUM_SAMPLES)
    my_buf=[]
    count=0
    print("Please speak:")
    while count<TIME*5:
        string_audio_data = stream.read(NUM_SAMPLES)
        my_buf.append(string_audio_data)
        count+=1
    save_wave_file('01.wav',my_buf)
    stream.close()
    model_param = LASR_SPEECH_PATH + '/src/model.out'
    name = task_predict("01.wav", "model.out")
    os.remove('01.wav')
    return name
if __name__ == '__main__':
    my_record()
    os.remove("01.wav")
